class Caja:
    def __init__(self):
        self.largo=0
        self.ancho=0
        self.costo=0

    def perimetro(self):
        return 2*(self.largo+self.ancho)

    def area(self):
        return self.largo*self.ancho

    def precio(self):
        area=self.area()
        return area*self.costo

    def ingresarNumero(self, x,y,z):
        self.largo=x
        self.ancho=y
        self.costo=z
        return x, y, z

miCaja=Caja()
miCaja.ingresarNumero(2,4,3)
miCaja.perimetro()
miCaja.area()
miCaja.precio()

print("Los números ingresados para largo, ancho y costo fueron: ")
print(miCaja.ingresarNumero(2,4,3))
print("El perimetro fue: ")
print(miCaja.perimetro())
print("El área fue: ")
print(miCaja.area())
print("El precio de los materiales fue: ")
print(miCaja.precio())